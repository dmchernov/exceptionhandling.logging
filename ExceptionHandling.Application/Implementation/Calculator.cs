﻿using System;
using Calculation.Interfaces;

namespace Calculation.Implementation
{
	public class Calculator : BaseCalculator, ICalculator
	{
		private readonly ILogger _logger;

		public Calculator(ILogger logger)
		{
			_logger = logger;
		}

		public int Sum(params int[] numbers)
		{
			_logger.Trace($"Вход в метод {nameof(Sum)}, параметры: {string.Join(", ", numbers)}");

			try
			{
				var sum = SafeSum(numbers);

				_logger.Info($"Метод {nameof(Sum)}, параметры: {string.Join(", ", numbers)}, результат: {sum}");

				return sum;
			}
			catch (Exception ex)
			{
				_logger.Error(ex);
				throw;
			}
			finally
			{
				_logger.Trace($"Выход из метода {nameof(Sum)}");
			}
		}

		public int Sub(int a, int b)
		{
			try
			{
				return SafeSub(a, b);
			}
			catch (Exception e)
			{
				throw e;
			}
		}

		public int Multiply(params int[] numbers)
		{
			try
			{
				return SafeMultiply(numbers);
			}
			catch (Exception e)
			{
				throw new InvalidOperationException("", e);
			}
		}

		public int Div(int a, int b)
		{
			try
			{
				return a / b;
			}
			catch
			{
				throw new InvalidOperationException();
			}
		}
	}
}